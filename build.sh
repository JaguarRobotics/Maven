#!/bin/bash

function buildProject() { # name, url
    echo "Building project $2 in $(pwd)/builds/$1/"
    if [ ! -d "$1" ]; then
        git clone "$2" "$1"
    fi
    cd "$1"
    for tag in $(git tag); do
        git tag -d $tag
    done
    git fetch
    for version in $(git tag); do
        echo "Building project version $version"
        git checkout $version
        if [ ! -d "../../caches/$1/$version" ]; then
            mkdir -p "../../caches/$1/$version"
        fi
        rm -rf build
        ln -s "../../caches/$1/$version" build
        ./gradlew publishToMavenLocal
    done
    cd ".."
    echo
}

function postProcessing() {
    for file in $(find public -name maven-metadata-local.xml); do
        md5sum "$file" | cut -d " " -f 1 | tr -d "\n" > "$(dirname "$file")/maven-metadata.xml.md5"
        sha1sum "$file" | cut -d " " -f 1 | tr -d "\n" > "$(dirname "$file")/maven-metadata.xml.sha1"
        mv "$file" "$(dirname "$file")/maven-metadata.xml"
    done
}

mkdir -p public builds caches gradle
rm -rf ~/.gradle ~/.m2/
mkdir -p ~/.m2/
ln -s "$(pwd)/public" ~/.m2/repository
ln -s "$(pwd)/gradle" ~/.gradle
cd builds
cat $(find ../repositories.list.d -type f) | sort | uniq | while read url; do
    buildProject "$(basename "$url" | sed -e "s|\.git$||")" "$url"
done
cd ..
postProcessing
